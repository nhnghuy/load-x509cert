﻿using System;
using System.IO;
using System.Text;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;

namespace net_core_console
{
    class Program
    {

        public const string PKCS12_FILEPATH = "FPT_pkcs12.p12";
        public const string PKCS12_PROTECTED_PASSWORD = "+KJdGz2L8+pSGizF8O8k4KP20rjma65LN36Cw4eqR1yg6MFvh2VPfw==";
        public const string PKCS12_PROTECTED_PASSWORD_KEY = "!@#$%^&*()~_+|";

        static void Main(string[] args)
        {
            var cipherTextBuffer = Convert.FromBase64String(PKCS12_PROTECTED_PASSWORD);
            var symKeyBuffer = Encoding.Unicode.GetBytes(PKCS12_PROTECTED_PASSWORD_KEY);
            var plainPKCS12Password = Decrypt(cipherTextBuffer, symKeyBuffer);

            Console.WriteLine($"PKCS12 Password : {plainPKCS12Password}");

            // var cert1 = X509Certificate.CreateFromSignedFile(PKCS12_FILEPATH) as X509Certificate2;

            // Console.WriteLine($"Private key : {cert1.PrivateKey}");

            var pkcs12Buffer = File.ReadAllBytes(PKCS12_FILEPATH);

            var cert2 = new X509Certificate2(pkcs12Buffer, plainPKCS12Password);

            Console.WriteLine($"Expiration Date : {cert2.GetExpirationDateString()}");
            Console.WriteLine($"Public key : {cert2.GetPublicKeyString()}");
            Console.WriteLine($"Private key : {cert2.PrivateKey.ToXmlString(true)}");
        }

        static string Decrypt(byte[] cipherTextBuffer, byte[] symKeyBuffer)
        {
            byte[] keyHashBuffer;
            using (var md5 = MD5.Create())
            {
                keyHashBuffer = md5.ComputeHash(symKeyBuffer);
            }

            using (var tdes = TripleDES.Create())
            {
                tdes.Mode = CipherMode.ECB;
                var decryptor = tdes.CreateDecryptor(keyHashBuffer, tdes.IV);
                var plainTextBuffer = decryptor.TransformFinalBlock(cipherTextBuffer, 0, cipherTextBuffer.Length);
                return Encoding.Unicode.GetString(plainTextBuffer);
            }
        }
    }
}
